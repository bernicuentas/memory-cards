/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memorycards.views;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import memorycards.controller.Controller;
import memorycards.models.Card;

/**
 *
 * @author Bernat
 */
public class memoryCardGame implements ActionListener{
    Controller controller = new Controller();
    private JFrame mainFrame;
    private Container mainContentPane;
    private ImageIcon cardIcon[];
    JPanel panel = new JPanel(new GridLayout(3,4));
    int cardsToAdd[] = new int[12];
    String fileName;
    InputStream imgStream;
    BufferedImage myImg;
    
    public memoryCardGame() {
        this.mainFrame = new JFrame("Memory Game");
        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.mainFrame.setSize(800,975);
        this.mainContentPane = this.mainFrame.getContentPane();
        this.mainContentPane.setLayout(new BoxLayout(this.mainContentPane, BoxLayout.PAGE_AXIS));
        
        //menu bar
        JMenuBar menuBar = new JMenuBar();
        this.mainFrame.setJMenuBar(menuBar);
        //game menu
        JMenu gameMenu = new JMenu ("Game");
        menuBar.add(gameMenu);
        //we need to create generic submenu creater
        newMenuItem("Exit", gameMenu, this);
        //about menu
        JMenu aboutMenu = new JMenu("About");
        menuBar.add(aboutMenu);
        newMenuItem("Help", aboutMenu, this);
        newMenuItem("About", aboutMenu, this);
        newMenuItem("etc...", aboutMenu, this);
        
        //we need to load cards
        this.cardIcon = loadCardIcons();
    }
    
        private ImageIcon[] loadCardIcons() {
        ImageIcon icon[] = new ImageIcon[7];
        for (int i = 0; i < 7; i++) {
            fileName = "/memorycards/card" + i + ".png";
            imgStream = memoryCardGame.class.getResourceAsStream(fileName );
            try {
                myImg = ImageIO.read(imgStream);
                icon[i] = new ImageIcon(myImg);
            } catch (IOException ex) {
                Logger.getLogger(memoryCardGame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return icon;
    }
        
    public void newGame()  {
        this.mainContentPane.removeAll();
        this.mainContentPane.add(makeCards());
        this.mainFrame.setVisible(true);
    }
    
    private void newMenuItem(String string, JMenu menu, ActionListener listener) {
        JMenuItem newItem = new JMenuItem(string);
        newItem.setActionCommand(string);
        newItem.addActionListener(listener);
        menu.add(newItem);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().equals("Exit")) {
            System.exit(0);
        }
    }
    
    public JPanel makeCards() {
        //all the vards have same back side
        Controller controller = new Controller();
        ImageIcon backIcon = this.cardIcon[6];
        cardsToAdd = new int[12];
        for (int i = 0; i < 6; i++) {
            cardsToAdd[2*i] = i;
            cardsToAdd[2*i+1] = i;
        }
        
        //lets randomize
        randomizeCardArray(cardsToAdd);
        //make card object
        for (int i = 0; i < cardsToAdd.length; i++) {
            int num = cardsToAdd[i];
            Card newCard = new Card(controller, this.cardIcon[num], backIcon, num);
            panel.add(newCard);
        }
        return panel;
    }

    private void randomizeCardArray(int[] t) {
        Random randomizer = new Random();
        for (int i = 0; i < t.length; i++) {
            int d = randomizer.nextInt(t.length);
            //swap
            int s = t[d];   
            t[d] = t[i];
            t[i] = s;
        }
    }
}
