/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memorycards.models;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JLabel;
import memorycards.controller.Controller;

/**
 *
 * @author Bernat
 */
public class Card extends JLabel implements MouseListener {
    Icon faceIcon;
    Icon backIcon;
    boolean faceUp = false;
    int num;
    int iconWidthHalf, iconHeightHalf;
    boolean mousePressedOnMe = false;
    private final Controller controller;
    
    public Card (Controller controller, Icon face, Icon back, int num) {
        super(back);
        this.faceIcon = face;
        this.backIcon = back;
        this.num = num;
        //catch mouse clicks
        this.addMouseListener(this);
        this.iconHeightHalf = back.getIconHeight()/2;
        this.iconWidthHalf = face.getIconWidth()/2;
        this.controller = controller;
    }

    public int getNum() {
        return num;
    }
    
    private boolean overIcon(int x, int y){
        int distX = Math.abs(x - (this.getWidth()/2));
        int distY = Math.abs(y - (this.getHeight()/2));
        //outside
        if (distX > this.iconHeightHalf || distY > this.iconWidthHalf) {
            return false;
        }
        //inside
        return true;
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (overIcon(me.getX(), me.getY())) {
            this.turnUp();
        }
    }
    
    public void turnUp() {
        if (this.faceUp) {
            return;
        }
        this.faceUp = true;
        this.faceUp = this.controller.turnUp(this);
        if (this.faceUp) {
            this.setIcon(this.faceIcon);
        }
    }
    
    public void turnDown() {
        if (!this.faceUp) {
            return;
        }
        this.setIcon(this.backIcon);
        this.faceUp = false;
    }
    
    @Override
    public void mousePressed(MouseEvent me) {
        if (overIcon(me.getX(), me.getY())) {
            this.mousePressedOnMe = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        if (this.mousePressedOnMe) {
            this.mousePressedOnMe = false;
            this.mouseClicked(me);
        }
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        this.mousePressedOnMe = false;
    }
}
