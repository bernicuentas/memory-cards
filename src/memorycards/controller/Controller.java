/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memorycards.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.Timer;
import memorycards.models.Card;

/**
 *
 * @author Bernat
 */
public class Controller implements ActionListener {
    private Vector turnedCards;
    private Timer turnDownTimer;
    private final int turnDownDelay = 1000;
    
    public Controller() {
        this.turnedCards = new Vector(2);
        this.turnDownTimer = new Timer(this.turnDownDelay, this);
        this.turnDownTimer.setRepeats(false);
    }
    
    public boolean turnUp (Card card) {
        if (this.turnedCards.size() < 2) {
            return doAddCard(card);
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        for (int i = 0; i < this.turnedCards.size(); i++) {
            Card card = (Card)this.turnedCards.get(i);
            card.turnDown();
        }
        this.turnedCards.clear();
    }

    private boolean doAddCard(Card card) {
        this.turnedCards.add(card);
        if (this.turnedCards.size() == 2) {
            Card otherCard = (Card)this.turnedCards.get(0);
            if (otherCard.getNum() == card.getNum()) {
                this.turnedCards.clear();
            } else {
                this.turnDownTimer.start();
            }
        }
        return true;
    }
}
